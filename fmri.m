
load fmri_words.mat;
    
[Y_new, I] = sort(Y_train);
X_new = X_train(I, :);
X_new = X_new(1:290, :);
semantic1 = word_features_centered(1:58, :);
%semantic2 = word_features_std(1:58, :);

index = 1:290;
index = floor((index-1)/5)+1;
semantic1 = semantic1(index, :);
%semantic2 = semantic2(index, :);

w = rand(length(X_train), 218);
w_new = w;

printf("Initialized random w\n");

function c = cost(X, y, theta),
    c = sum((X*theta-y).^2);
end

function [theta, CostHistory] = gradientDescent(X, theta, y, alpha, numIters)
% This is the gradient descent fucntion.

% Initialize values
m = length(y); % number of training examples
CostHistory = zeros(numIters, 1);
thetaLen = length(theta);
tempVal = theta; % Just a temporary variable to store theta values.
    for iter=1:numIters
        temp = (X*theta - y);
        
        for i=1:thetaLen
            tempVal(i,1) = sum(temp.*X(:,i));
        end
        
        theta = theta - (alpha/m)*tempVal;
        
        CostHistory(iter,1) = cost(X,y,theta);
     
    end
end

for i=1:218,
    [w(:, i), CostHistory] = gradientDescent(X_new, w(:, i), semantic1(:, i), 0.0001, 20);
    disp(i);
    
    %debug lines for gradient descent
    hold on;
    figure(i);
    plot(CostHistory);
    hold off;
    disp(CostHistory);
    if (i==1),
    disp(CostHistory);
    end
    
end


save('filename.mat', 'w');
% to not run gradient descent load filename.mat. It contains w matrix
% load filename.mat
acc1 = 0;   

for i = 1:60,
    predict = X_test(i, :)*w;
    dist1 = norm(word_features_centered(Y_test(i, 1), :)-predict);
    dist2 = norm(word_features_centered(Y_test(i, 2), :)-predict);

    if dist1 <= dist2,
        disp("Changing acc1");
        acc1 = acc1+1;
    end

    %dist = bsxfun(@minus, word_features_centered, predict);
    %dist = sum(abs(dist).^2, 2);
    %[~, idx] = min(dist);
    
    % printf('%d %d\n', idx, Y_test(i));
    % Y_test(i)

    %{
    printf('%d %d\n', idx, Y_test(i));
    predict = X_test(i, :)*w2;
    dist = bsxfun(@minus, word_features_std, predict);
    dist = sum(abs(dist).^2, 2);
    [~, idx] = max(dist);

    idx
    Y_test(i)
    if idx==Y_test(i, :) ,
        acc2 = acc2 + 1;
    end
    %}
end     

acc1 = (acc1/60)*100
