load('fmri_words.mat');
c_all = zeros(21764,218);
for i = 1:300
    A=transpose(X_train(i,:));
    C=A*word_features_centered(Y_train(i),:)/norm(word_features_centered(Y_train(i),:));;
    c_all=c_all+C;
end
c_all=c_all/300;
%size(c_all)
ans=0;
for j = 1:60
    fir_w = Y_test(j,1);
    sec_w = Y_test(j,2);
    img_fir = c_all*transpose(word_features_centered(fir_w,:));
    img_sec = c_all*transpose(word_features_centered(sec_w,:));
    right = X_test(j,:);
    %size(right)
    %size(img_fir)
    dot_fir=dot(right,transpose(img_fir))
    dot_sec=dot(right,transpose(img_sec))
    if dot_fir >= dot_sec
        ans=ans+1;
    end
end
ans
%accuracy is ans*100/60 
        
